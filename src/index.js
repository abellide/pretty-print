const chalk = require('chalk');
const cJson = require('circular-json');

const bold = true;
const level = {
    info: 'info',
    warn: 'warn',
    error: 'error'
};
const color = {
    black: 'black',
    red: 'red',
    green: 'green',
    yellow: 'yellow',
    blue: 'blue',
    magenta: 'magenta',
    cyan: 'cyan',
    white: 'white',
    gray: 'gray',
    redBright: 'redBright',
    greenBright: 'greenBright',
    yellowBright: 'yellowBright',
    blueBright: 'blueBright',
    magentaBright: 'magentaBright',
    cyanBright: 'cyanBright',
    whiteBright: 'whiteBright'
};

const getJson = (text, identation = 2) => cJson.stringify(text, undefined, identation);
const getChalk = (bold, c) => {
    switch (c) {
        case color.black:
            return bold ? chalk.bold.black : chalk.black;
        case color.red:
            return bold ? chalk.bold.red : chalk.red;
        case color.green:
            return bold ? chalk.bold.green : chalk.green;
        case color.yellow:
            return bold ? chalk.bold.yellow : chalk.yellow;
        case color.blue:
            return bold ? chalk.bold.blue : chalk.blue;
        case color.magenta:
            return bold ? chalk.bold.magenta : chalk.magenta;
        case color.cyan:
            return bold ? chalk.bold.cyan : chalk.cyan;
        case color.white:
            return bold ? chalk.bold.white : chalk.white;
        case color.gray:
            return bold ? chalk.bold.gray : chalk.gray;
        case color.redBright:
            return bold ? chalk.bold.redBright : chalk.redBright;
        case color.greenBright:
            return bold ? chalk.bold.greenBright : chalk.greenBright;
        case color.yellowBright:
            return bold ? chalk.bold.yellowBright : chalk.yellowBright;
        case color.blueBright:
            return bold ? chalk.bold.blueBright : chalk.blueBright;
        case color.magentaBright:
            return bold ? chalk.bold.magentaBright : chalk.magentaBright;
        case color.cyanBright:
            return bold ? chalk.bold.cyanBright : chalk.cyanBright;
        case color.whiteBright:
            return bold ? chalk.bold.whiteBright : chalk.whiteBright;
        default:
            return chalk.black;
    }
};

const getConsole = (lvl) => {
    switch (lvl) {
        case level.info:
            return console.log;
        case level.warn:
            return console.warn;
        case level.error:
            return console.error;
        default:
            return console.log;
    }
};

const printLvl = (text, color = 'white', bold = false, lvl = level.info , identation = 2) => {
    if (typeof text === 'object') {
        getConsole(lvl)(getChalk(bold, color)(getJson(text, identation)));
    } else {
        getConsole(lvl)(getChalk(bold, color)(text));
    }
};

const print = (text, color = 'white', bold = false, identation = 2) => {
    if (typeof text === 'object') {
        console.log(getChalk(bold, color)(getJson(text, identation)));
    } else {
        console.log(getChalk(bold, color)(text));
    }
};

const err = (text, color = 'white', bold = false, identation = 2) => printLvl(text, color, bold, level.error , identation);
const warn = (text, color = 'white', bold = false, identation = 2) => printLvl(text, color, bold, level.warn , identation);

module.exports = {
    bold,
    color,
    level,
    print,
    err,
    warn
};
