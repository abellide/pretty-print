# AWESOME-PRINTER

## Instalation
    npm i awesome-printer

## Usage

```javascript
const printer = require('awesome-printer');

printer.print('text');
printer.print('blue-text', printer.color.blue);
printer.print('blue-bold-text', printer.color.blue, printer.bold);

printer.err('whoops! a wild error appeared');
printer.warn('a warn is fine, i guess...'); // no a warn isn't good :D
```
**Its also works with exported objects !!!**
```javascript
const myObject = require('./myAwesomeObjectFile').object;
printer.print(myObject, printer.color.magenta);
```
And here is what you got:

![cool!](https://i.imgur.com/V7nnSVk.jpg)

### COLORS
* black
* red
* green
* yellow
* blue
* magenta
* cyan
* white
* gray
* redBright
* greenBright
* yellowBright
* blueBright
* magentaBright
* cyanBright
* whiteBright


## Authors
Álvaro Bellido Enguidanos

## License
MIT